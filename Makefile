.PHONY=lint compile triggerdocker triggerchart
CHART_NAME=$(shell cat charts/spin-helm-demo/Chart.yaml | yq -r .name)
CHART_VERSION=$(shell cat charts/spin-helm-demo/Chart.yaml | yq -r .version)

APP_VERSION ?= $(shell cat charts/spin-helm-demo/Chart.yaml | yq -r .appVersion)

DOCKER_REPO ?= registry.gitlab.com/ethanfrogers/spin-helm-demo
SPINNAKER_API ?= https://my-spinnaker.io

docker:
	docker build -t $(DOCKER_REPO):$(APP_VERSION) .
	docker tag $(DOCKER_REPO):$(APP_VERSION) $(DOCKER_REPO):latest

dockerpush: docker
	docker login -u gitlab-ci-token -p $(CI_BUILD_TOKEN) registry.gitlab.com
	docker push $(DOCKER_REPO):$(APP_VERSION)
	docker push $(DOCKER_REPO):latest

lint:
	helm lint charts/*

compile:
	mkdir -p ./public
	helm package charts/spin-helm-demo --destination public

triggerdocker:
	curl -L -vvv -X POST \
		-k \
		-H"Content-Type: application/json" $(SPINNAKER_API)/webhooks/webhook/gitlab \
		-d '{"artifacts": [{"type": "docker/image", "name": "$(DOCKER_REPO)", "reference": "$(DOCKER_REPO):$(APP_VERSION)", "kind": "docker"}]}'

triggerchart:
	curl -L -vvv -X POST \
		-k \
		-H"Content-Type: application/json" $(SPINNAKER_API)/webhooks/webhook/gitlab \
		-d '{"artifacts": [{"type": "gitlab/file", "name": "public/spin-helm-demo-0.1.0.tgz", "reference": "https://gitlab.com/ethanfrogers/spin-helm-demo/-/jobs/artifacts/master/raw/public/$(CHART_NAME)-$(CHART_VERSION).tgz?job=chart_repo", "kind": "gitlab"}]}'